<?php

namespace Drupal\book_store\Controller;

use Drupal\Core\Controller\ControllerBase; 
use Drupal\Core\Database\Database;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\JsonResponse;


class BookStoreController extends ControllerBase
{

    public function showBooks()
    {   

        $query = \Drupal::entityQuery('node');
        $query->condition('type', 'books');
        $entity_ids = $query->execute();
        $books = Node::loadMultiple($entity_ids);
        
        $booksToShow = [];

        foreach ($books as $value) {
            $booksToShow[]  = ['bookName' => $value->getTitle(),
                               'description' => $value->get('field_description')->value,
                               'author' => $value->get('field_book_author')->value,
                               'price' => $value->get('field_price')->value
                              ];
        }

        $build = [
            '#theme' => 'index',
            '#names' =>  $booksToShow,
            '#cache' => ['max-age' => 0]
                 ];
       
        return $build;
       
    }

    public function showBooksApi()
    {
        $query = \Drupal::entityQuery('node');
        $query->condition('type', 'books');
        $entity_ids = $query->execute();
        $books = Node::loadMultiple($entity_ids);
        
        $booksToShow = [];

        foreach ($books as $value) {
            $booksToShow[]  = ['bookName' => $value->getTitle(),
                               'description' => $value->get('field_description')->value,
                               'author' => $value->get('field_book_author')->value,
                               'price' => $value->get('field_price')->value
                              ];
        }
       
        return new JsonResponse($booksToShow);

    }

}
